function Invoke-PasswordGenerator {
    [CmdletBinding()]
    param(
        [Parameter(Position=0)]
        [string]$AddNumber = "true",
        [Parameter(Position=1)]
        [string]$AddUppercase = "true",
        [Parameter(Position=2)]
        [string]$MinLength = "25",
        [Parameter(Position=3)]
        [string]$MinWords = "4",
        [Parameter(Position=4)]
        [string]$Separator = "-"
    )
    $passGenReqBody = @{
        AddNumber = "$AddNumber";
        AddUppercase = "$AddUppercase";
        MinLength = "$MinLength";
        MinWords = "$MinWords";
        Separator = "$Separator";
    }
    $json = ($passGenReqBody) | ConvertTo-Json
    $passGenUri = "http://getanewpassword.com/api/generatepassword"
    $passGenParams = @{
        Body = $json;
        Method = "Get";
        Uri = $passGenUri;
        ContentType = "application/json";
    }
    $script:passGenReqData = Invoke-RestMethod @passGenParams -Verbose
    Write-Host ""
    Write-Output "Your password is: $($script:passGenReqData.Password)"
    $passGenReqBody
}
Clear-Host
Invoke-PasswordGenerator -AddNumber true -AddUppercase true -MinLength 20 -MinWords 3 -Separator "."